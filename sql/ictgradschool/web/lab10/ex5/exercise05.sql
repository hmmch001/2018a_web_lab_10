-- Answers to Exercise 5 here

DROP TABLE IF EXISTS Exercise5;
CREATE TABLE IF NOT EXISTS Exercise5 (
  username VARCHAR(20),
  first_name VARCHAR(20),
  last_name VARCHAR(30),
  email VARCHAR(100),
  PRIMARY KEY  (username)
);

INSERT INTO Exercise5 (username, first_name, last_name, email) VALUES
  ('programmer1', 'Bill', 'Gates', 'bill@microsoft.com'),
  ('PGates', 'Pete', 'Gates', 'pete@microsoft.com'),
  ('Smithy', 'Peter', 'Smith', 'peter@microsoft.com'),
  ('DaveP', 'Dave', 'Peterson', 'dave@microsoft.com'),
  ('programmer6', 'Bob', 'Gates', 'bob@microsoft.com');