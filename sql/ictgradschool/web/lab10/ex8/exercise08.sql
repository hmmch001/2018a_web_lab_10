-- Answers to Exercise 8 here
-- delete row from ex 5 table
DELETE FROM Exercise5 WHERE first_name = 'Bill';
-- delete a column from ex 5
ALTER TABLE Exercise5 DROP COLUMN last_name;
-- delete entire ex 5 table
DROP TABLE Exercise5;
-- change a value that is a string
UPDATE Exercise5 SET first_name = 'George' WHERE username = 'programmer1';
-- change a value that is a numeric value
UPDATE Exercise6 SET weekly_rate = '1.00' WHERE director = 'Tim Burton';
-- change a value that is a primary key
UPDATE Exercise6 SET barcode = '0000' WHERE title = 'Dark Shadows';






