-- Answers to Exercise 2 here
DROP TABLE IF EXISTS Exercise2;
CREATE TABLE IF NOT EXISTS Exercise2 (
  id INT NOT NULL AUTO_INCREMENT,
  username VARCHAR(20),
  first_name VARCHAR(20),
  last_name VARCHAR(30),
  email VARCHAR(100),
  PRIMARY KEY  (id)
);

INSERT INTO Exercise2 (username, first_name, last_name, email) VALUES
  ('programmer1', 'Bill', 'Gates', 'bill@microsoft.com'),
  ('PGates', 'Pete', 'Gates', 'pete@microsoft.com'),
  ('Smithy', 'Peter', 'Smith', 'peter@microsoft.com'),
  ('DaveP', 'Dave', 'Peterson', 'dave@microsoft.com'),
  ('programmer1', 'Bob', 'Gates', 'bob@microsoft.com');
