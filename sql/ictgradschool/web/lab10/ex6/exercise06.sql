-- Answers to Exercise 6 here
DROP TABLE IF EXISTS Exercise6;

CREATE TABLE IF NOT EXISTS Exercise6(
  barcode INT,
  title VARCHAR(100),
  director VARCHAR(50),
  weekly_rate REAL,
  on_loan_to VARCHAR(100),
  PRIMARY KEY (barcode),
  FOREIGN KEY (on_loan_to) references Exercise3 (name)
);

INSERT INTO Exercise6 (barcode, title, director, weekly_rate, on_loan_to) VALUES

('1234', 'Dark Shadows', 'Tim Burton', '2.00','Peter Jackson'),
('5678', 'Walk the Line', 'James Mangold', '4.00', 'Jane Campion'),
('1567', 'The Help', 'Tate Taylor', '6.00', 'Roger Donaldson'),
('4967', 'Meet the Parents', 'Jay Roach', '2.00', 'Temuera Morrison'),
('1111', 'The Kings Speech', 'Tom Hooper', '4.00', 'Lucy Lawless'),
('3333', 'Charlie and the Chocolate Factory', 'Tim Burton', '6.00', 'Kiri Te Kanawa'),
('2222', 'Alice In Wonderland', 'Tim Burton', '4.00', 'Andrew Niccol'),
('4444', 'Brokeback Mountain', 'Ang Lee', '2.00', 'Michael Hurst'),
('5555', 'Gladiator', 'Ridley Scott', '6.00', 'Russell Crowe'),
('6666', 'The Parent Trap', 'David Swift', '4.00', 'Lorde');
