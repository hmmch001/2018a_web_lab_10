-- Answers to Exercise 9 here
-- All information about all the members of the video store
SELECT * FROM Exercise3;
-- Everything about all the members of the video store, except the number of video hires they have
SELECT name, gender, year_born, joined FROM Exercise3;
-- All the titles to the articles that have been written
SELECT title FROM Exercise4;
-- All the directors of the movies the video store has (without repeating any names).
SELECT DISTINCT director FROM Exercise6;
-- All the video titles that rent for $2 or less a week.
SELECT title FROM Exercise6 WHERE weekly_rate <= '2.00';
--  A sorted list of all the usernames that have been registered.
SELECT username FROM Exercise5 ORDER BY username;
-- All the usernames where the user’s first name starts with the letters ‘Pete’.
SELECT username FROM Exercise5 WHERE first_name LIKE 'Pete%';
-- All the usernames where the user’s first name or last name starts with the letters ‘Pete’.
SELECT username FROM Exercise5 WHERE first_name LIKE 'Pete%' or last_name LIKE 'Pete%';