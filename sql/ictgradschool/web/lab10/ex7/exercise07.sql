-- Answers to Exercise 7 here
DROP TABLE IF EXISTS Exercise7;

CREATE TABLE IF NOT EXISTS Exercise7(
  id INT NOT NULL AUTO_INCREMENT,
  comments VARCHAR(1000),
  PRIMARY KEY  (id),
  FOREIGN KEY (comments) references Exercise4 (id)
);